<?php

namespace App\Commands;

use Redis;
use App\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;

class RedisSubscribe extends Command implements SelfHandling
{
    /**
     * Name and signature of the console command
     * @var string
     */
    protected $signature = 'redis:subscribe';

    /**
     * Console command description.
     * 
     * @var string
     */
    protected $description = 'Subscribe to Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        Redis::subscribe(['channel'], function($message) {
            echo $message;
        });
    }
}
